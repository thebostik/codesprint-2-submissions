import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {

	public static int count = 0;

	/**
	 * Launch the Solution class
	 * 
	 * @param args
	 *            Contains nothing of importance
	 */
	public static void main(String... args) {

		int numTests = -1;
		int iTest = 0;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String currLine = "";

			int numNums = -1;
			ArrayList<String> rawInput = new ArrayList<String>();
			while ((currLine = br.readLine()) != null) {
				if (numTests < 0) {
					numTests = new Integer(currLine);
				}
				else
					rawInput.add(currLine);
				
				if (rawInput.size() == numTests * 2)
					break;
			}
			for (String line : rawInput) {
				// find out the number of test cases

				if (numNums < 0) {
					numNums = new Integer(line);
				} else {
					iTest++;

					// read and execute each test case
					System.out.println(executeTestCase(line, numNums));
					count = 0;
					if (iTest >= numTests) {
						System.exit(0);
					}

					numNums = -1;
				}
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param input
	 *            A single test case coming from STDIN
	 * @return The string which is output to STDOUT
	 */
	public static String executeTestCase(String input, int numNums) {
		String[] inputs = input.split(" ");

		int[] numbers = new int[numNums];
		int i = 0;
		for (String x : inputs) {
			numbers[i] = new Integer(x);
			i++;
		}

		countWays(numbers);
		return "" + count % 1000000007;
	}

	// invoke the function for permuting the input numbers
	public static void countWays(int[] nums) {
		
		
		
		
		/* Comment the previous code in order to attempt a speedup */
		//perm2(nums, nums.length);
	}

	// Following is a simple implementation of java permutation from
	// http://introcs.cs.princeton.edu/java/23recursion/Permutations.java.html
	private static void perm2(int[] a, int n) {
		if (n == 1) {
			if (isValid((a))) {
				count++;
			}
			return;
		}
		for (int i = 0; i < n; i++) {
			swap(a, i, n - 1);
			perm2(a, n - 1);
			swap(a, i, n - 1);
		}
	}

	// swap the characters at indices i and j
	private static void swap(int[] a, int i, int j) {
		int c;
		c = a[i];
		a[i] = a[j];
		a[j] = c;
	}

	/**
	 * Given a single permutation, return true if the cards can be picked up in
	 * this order.
	 * 
	 * @param permutation
	 *            The input permutation
	 * @return Whether the ordering of cards is valid
	 */
	public static boolean isValid(int[] permutation) {
		for (int i = 0; i < permutation.length; i++) {
			if (permutation[i] > i) {
				return false;
			}
		}

		return true;
	}

}