import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * A FeatureMap is a helper class to hold each feature processed, as well as
 * helper methods associated with processing this same feature in the query set
 * 
 * @author stephen
 * 
 */
class FeatureMap {
	int featureId;
	TreeMap<Double, Integer> featureValues;
	int threshold_high;
	int threshold_low;
	float threshold_certainity = .18f;
	boolean positiveCorrelation = true;
	
	boolean goodFeature = false;
	
	FeatureMap(int id) {
		featureId = id;
		featureValues = new TreeMap<Double, Integer>();
	}

	void setGoodFeature() {
		// does an analysis of the data attached to this feature,
		// and if there seems to be a correlation, then sets goodFeature = true
		
		int index = 0;
		int sum = 0;
		for (Integer vote : featureValues.values()) {
			sum += vote;
			index ++;
			if (index > featureValues.size() / 2)
				break;
		}
		
		if (Math.abs((double) sum) > featureValues.size()/4)
			goodFeature = true;
	}
	
	void findThreshold() {
		if (!goodFeature) {
			return;
		}
		// 1) Iterate through the feature map,
		// 2) Set low/high thresholds
		
		// Set low threshold
		int wrong = 0;
		int i = 0;
		for (Double d : featureValues.keySet()) {
			if (wrong > threshold_certainity * featureValues.size()) {
				threshold_low = i;
				break;
			}

			Entry<Double, Integer> floorEntry = featureValues.lowerEntry(d);
			if (floorEntry == null)
				continue;
			if (floorEntry.getValue() != featureValues.get(d))
				wrong++;
			i++;
		}
		
		// set high threshold
		wrong = 0;
		i = featureValues.size();
		for (Double d : featureValues.descendingKeySet()) {
			if (wrong > threshold_certainity * featureValues.size()) {
				threshold_high = i;
				break;
			}

			Entry<Double, Integer> ceilingEntry = featureValues.higherEntry(d);
			if (ceilingEntry == null)
				continue;
			if (ceilingEntry.getValue() != featureValues.get(d))
				wrong++;
			i--;
		}

		// if high < low, switch the correlation
		if (threshold_high < threshold_low) {
			positiveCorrelation = false;
		}
	}

	int getFeatureEstimate(double query) {
		// Must first invoke FeatureMap.findThreshold()
		if (!goodFeature) {
			return 0;
		}
		if (positiveCorrelation) {
			if (query < threshold_low)
				return 1;
			else if (query > threshold_high)
				return -1;
		} else {
			if (query < threshold_low)
				return -1;
			else if (query > threshold_high)
				return 1;
		}
		return 0;
	}

	@Override
	public String toString() {
		return "\nFeatureMap [threshold_high=" + threshold_high
				+ ", threshold_low=" + threshold_low + ", goodFeature="
				+ goodFeature + "]";
	}

}



class SingleResult {
	String name;
	Integer result;
	Integer rawResult;
	
	SingleResult(String name, Integer rawResult) {
		this.name = name;
		this.rawResult = rawResult;
	}
	
	void setNormalizedResult(float mean) {
		if (rawResult < mean)
			result = -1;
		else
			result = 1;
	}
}


// Wish I could submit multiple files... ho
// hum..---------------------------------------
public class Solution {
	static ArrayList<FeatureMap> allFeatures = new ArrayList<FeatureMap>();
	static ArrayList<SingleResult> allResults = new ArrayList<SingleResult>();
	
	static String result = "";

	public static void main(String... args) {
		int iN = -1;
		int iM = -1;
		int iQ = -1;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String currLine = "";
			ArrayList<String> trainingInput = new ArrayList<String>();
			ArrayList<String> queryInput = new ArrayList<String>();

			// Read Training Data
			while ((currLine = br.readLine()) != null) {
				if (iN < 0) {
					String[] counts = currLine.split(" ");
					iN = new Integer(counts[0]);
					iM = new Integer(counts[1]);
				} else
					trainingInput.add(currLine);

				if (trainingInput.size() == iN)
					break;
			}

			// Read Query Data
			while ((currLine = br.readLine()) != null) {
				if (iQ < 0) {
					iQ = new Integer(currLine);
				} else
					queryInput.add(currLine);

				if (queryInput.size() == iQ)
					break;
			}

			// Initialize all Feature Maps
			for (int i = 1; i <= iM; i++) {
				allFeatures.add(new FeatureMap(i));
			}

			// Parse training input, Load features HashMap
			for (String line : trainingInput) {
				loadTrainingData(line, iM);
			}

			// Calculate thresholds for each feature in training data
			for (FeatureMap f : allFeatures) {
				f.setGoodFeature();
				f.findThreshold();
			}

			//System.err.println(allFeatures); // EVERYTHING LOOK VERY NICE I LIKE

			// Load query input / predict vote based on all features
			for (String line : queryInput) {
				loadQueryData(line, iM);
			}

			// Normalize the predictions
			float average = findAveragePrediction(allResults);
			
			// Compile the answer output
			for (SingleResult sr : allResults) {
				sr.setNormalizedResult(average);
				result += sr.name + " " + String.format("%+d",sr.result) + "\n";
			}
						
			// Print the answer (minus the final trailing newline
			System.out.println(result.substring(0, result.length() - 1));

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadTrainingData(String input, int featureCount) {
		String[] brokenInput = input.split(" ");

		String answerId = brokenInput[0];
		String strAnswerVote = brokenInput[1];
		int answerVote;
		if (strAnswerVote.charAt(0) == '+')
			answerVote = new Integer(strAnswerVote.substring(1,
					strAnswerVote.length()));
		else
			answerVote = new Integer(strAnswerVote);

		for (int i = 0; i < featureCount; i++) {
			String[] featureFormat = brokenInput[2 + i].split(":");
			allFeatures.get(i).featureValues.put(new Double(featureFormat[1])
					+ Math.random() / 100000., answerVote);
		}
	}
	
	public static float findAveragePrediction(ArrayList<SingleResult> toAverage) {
		ArrayList<Integer> rawData = new ArrayList<Integer>();
		
		for (SingleResult sr : allResults) {
			rawData.add(sr.rawResult);
		}
		int from, to;
		from = (int) (toAverage.size() * .2);
		to = (int) (toAverage.size() * .8);
		Collections.sort(rawData);
		List<Integer> sortedShreddedData = rawData.subList(from, to);
		
		float sum = 0, average = 0;
		for (Integer i : sortedShreddedData) {
			sum += i;
		}
		average = sum / sortedShreddedData.size();
		return average;
		
	}
	
	public static void loadQueryData(String input, int featureCount) {
		String[] brokenInput = input.split(" ");
		String answerId = brokenInput[0];

		int prediction = 0;

		for (int i = 0; i < featureCount; i++) {
			String[] featureFormat = brokenInput[1 + i].split(":");

			// Figure out if this feature predicts +/- vote
			prediction += allFeatures.get(i).getFeatureEstimate(
					new Double(featureFormat[1]));
		}

		// Save the raw predictive score
		allResults.add(new SingleResult(answerId, prediction));
		//System.err.println("Raw Score -- " + answerId + ": " + prediction);
	}
}
