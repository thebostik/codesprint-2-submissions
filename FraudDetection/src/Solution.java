import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class LineItem {

	@Override
	public String toString() {
		return "LineItem [orderId=" + orderId + ", dealId=" + dealId
				+ ", email=" + email + ", address=" + address + ", cc=" + cc
				+ "]" + "\n";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineItem other = (LineItem) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (cc == null) {
			if (other.cc != null)
				return false;
		} else if (!cc.equals(other.cc))
			return false;
		if (dealId == null) {
			if (other.dealId != null)
				return false;
		} else if (!dealId.equals(other.dealId))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	String orderId;
	String dealId;
	String email;
	String address;
	String cc;

	LineItem(String input) {
		String[] fields = input.split(",");

		this.orderId = new Integer(fields[0]).toString();
		this.dealId = new Integer(fields[1]).toString(); // if this fails, field
															// is not numeric
		this.email = Solution.stripEmail(fields[2]);
		this.address = Solution.stripAddress(fields[3])
				+ fields[4].toLowerCase()
				+ Solution.stripState(fields[5])
				+ fields[6].toLowerCase();
		
		this.cc = fields[7];

	}

}

public class Solution {

	public static HashMap<String, Set<LineItem>> addressMap = new HashMap<String, Set<LineItem>>();
	public static HashMap<String, Set<LineItem>> emailMap = new HashMap<String, Set<LineItem>>();
	public static HashSet<Integer> unorderedFrauds = new HashSet<Integer>();

	public static void main(String... args) {
		int numTests = -1;
		int iTest = 0;
		StringBuilder result = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String currLine = "";

			while ((currLine = br.readLine()) != null) {
				// find out the number of test cases
				if (numTests < 0) {
					numTests = new Integer(currLine);
				} else {
					iTest++;

					// read and execute each test case
					executeTestCase(currLine);

					if (iTest >= numTests) {
						// System.out.println(emailMap);
						// System.out.println(addressMap);
						break;
					}
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Sort the HashSet of fraudulent orders
		Integer[] orderedFrauds = {};
		orderedFrauds = unorderedFrauds.toArray(orderedFrauds);

		Arrays.sort(orderedFrauds);
		for (Integer x : orderedFrauds) {
			result.append(x + ",");
		}
		if (result.length() > 0)
			System.out.println(result.substring(0, result.length() - 1));
		else
			System.out.println();
	}

	/**
	 * 
	 * @param input
	 *            A single test case coming from STDIN
	 * @return The string which is output to STDOUT
	 */
	public static void executeTestCase(String input) {
		// Apply data cleansing logic to the row of input
		LineItem li = null;
		try {
			li = new LineItem(input);
		} catch (Exception e) {
			return;
		}

		// Add to the data structure
		Set<LineItem> oldOrders;
		if (emailMap.containsKey(li.email + " " + li.dealId)) {
			oldOrders = emailMap.get(li.email + " " + li.dealId);
		} else
			oldOrders = new HashSet<LineItem>();

		for (LineItem fraudI : oldOrders) {
			if (check_fraud(fraudI, li)) {
				unorderedFrauds.add(new Integer(li.orderId));
				unorderedFrauds.add(new Integer(fraudI.orderId));
			}
		}

		oldOrders.add(li);

		emailMap.put(li.email + " " + li.dealId, oldOrders);

		oldOrders = null;
		if (addressMap.containsKey(li.address + " " + li.dealId)) {
			oldOrders = addressMap.get(li.address + " " + li.dealId);
		} else
			oldOrders = new HashSet<LineItem>();

		for (LineItem fraudI : oldOrders) {
			if (check_fraud(fraudI, li)) {
				unorderedFrauds.add(new Integer(li.orderId));
				unorderedFrauds.add(new Integer(fraudI.orderId));
			}
		}

		oldOrders.add(li);

		addressMap.put(li.address + " " + li.dealId, oldOrders);

	}

	public static boolean check_fraud(LineItem a, LineItem b) {
		if (a.cc.equals(b.cc))
			return false;
		if (a.orderId.equals(b.orderId))
			return false;
		if (!a.dealId.equals(b.dealId))
			return false;
		return true;
	}

	/**
	 * 
	 * @param email
	 *            The raw user-submitted email address
	 * @return The data-cleansed email with any cleansing rules applied
	 */
	public static String stripEmail(String email) {
		String user = email.substring(0, email.indexOf('@'));
		user = user.replace(".", "");
		if (user.indexOf('+') >= 0) {
			user = user.substring(0, user.indexOf('+'));
		}

		return (user + email.substring(email.indexOf('@'))).toLowerCase();
	}

	/**
	 * 
	 * @param address
	 *            The raw user-submitted address
	 * @return The data-cleansed address with any cleansing rules applied
	 */
	public static String stripAddress(String address) {
		return address.toLowerCase().replace("st.", "street")
				.replace("rd.", "road");
	}

	public static String stripState(String state) {
		return state.toLowerCase().replace("illinois", "il")
				.replace("new york", "ny").replace("california", "ca");
	}

}
