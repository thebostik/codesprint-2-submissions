import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Solution {

	public static void main(String... args) {
		int numTests = -1;
		int iTest = 0;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String currLine = "";
			String strResult = "";
			while ((currLine = br.readLine()) != null) {
				// find out the number of test cases
				if (numTests < 0) {
					numTests = new Integer(currLine);
				} else {
					iTest++;

					// read and execute each test case
					strResult += executeTestCase(currLine);

					if (iTest >= numTests) {
						System.out.println(strResult);
						System.exit(0);
					} else
						strResult += "\n";

				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param input
	 *            A single test case coming from STDIN
	 * @return The string which is output to STDOUT
	 */
	public static String executeTestCase(String input) {
		String[] inputs = input.split(" ");
		String result = findExpectedValue(new Integer(inputs[0]), new Integer(
				inputs[1]));

		return result + ".00";
	}

	/**
	 * 
	 * @param n
	 *            Total number of tosses needed with heads up
	 * @param m
	 *            Number of tosses completed with heads up
	 * @return
	 */
	public static String findExpectedValue(int n, int m) {
		// 2^(n+1) - 2
		// http://www.qbyte.org/puzzles/p082s.html

		if (n == m) {
			return "0";
		}
		
		if (m > 0)
			return findExpectedValue(n).subtract(findExpectedValue(m)).toString();
		else
			return findExpectedValue(n).toString();
	}
	
	public static BigInteger findExpectedValue(int n) {
		BigInteger bi2 = new BigInteger("2");
		BigInteger result = bi2.pow(n+1).subtract(bi2);  
		
		return result;
	}
}
