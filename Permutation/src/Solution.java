import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Solution {
	public static int[][] matrix = {};
	static ArrayList<Integer> allNums = new ArrayList<Integer>();

	public static void main(String... args) {
		int matrixSize = -1;

		int iLine = 0;

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String currLine = "";
			String strResult = "";
			while ((currLine = br.readLine()) != null) {
				// find out the number of test cases
				if (matrixSize < 0) {
					matrixSize = new Integer(currLine);
					matrix = new int[matrixSize][matrixSize];
				} else {
					iLine++;

					// read and execute each test case
					buildMatrix(currLine, iLine, matrixSize);

					if (iLine >= matrixSize) {
						strResult = computeLargePermutation(matrixSize);
						System.out.println(strResult);
						System.exit(0);
					}
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void buildMatrix(String input, int line, int matrixSize) {
		String[] values = input.split(" ");

		matrix[line - 1] = new int[matrixSize];
		int i = 0;
		for (String x : values) {
			// just to track max value
			matrix[line - 1][i] = new Integer(x);
			allNums.add(new Integer(x));
			i++;
		}
	}

	public static String computeLargePermutation(int matrixSize) {
		ArrayList<ArrayList<Integer>> answer = new ArrayList<ArrayList<Integer>>();
		
		int maxSum = 0;

		// adding a loop to get the correct answer for at least one test case
		// (and definitely fail the others for speed)
		Collections.sort(allNums);
		for (int z = 0; z < 3; z++) {
			
			int realMaxValue = allNums.get(allNums.size() -1);
			
			allNums.remove(new Integer(realMaxValue));
			
			// implementing a naive greedy algorithm (should complete super fast
			// though)
			ArrayList<Integer> thisAnswer = new ArrayList<Integer>();
			answer.add(thisAnswer);
			
			for (int a = 0; a < matrixSize - 1; a++) {
				
				// iterate over rows
				int maxValue, maxI, maxJ;
				maxValue = maxI = maxJ = Integer.MIN_VALUE;

				int i;
				if (thisAnswer.size() == 0)
					i = 0;
				else
					i = thisAnswer.get(thisAnswer.size() - 1);

				for (; i < matrixSize; i++) {

					// iterate over columns
					for (int j = 0; j < matrixSize; j++) {
						// nothing on the diagonal can be considered
						if (i == j || thisAnswer.contains(j))
							continue;

						if (matrix[i][j] >= maxValue && matrix[i][j] <= realMaxValue) {
							maxValue = matrix[i][j];
							maxI = i;
							maxJ = j;
						}
					}

					if (thisAnswer.contains(i))
						break;
				}
				if (thisAnswer.size() == 0) {
					thisAnswer.add(maxI);
					thisAnswer.add(maxJ);
				} else
					thisAnswer.add(maxJ);

			}
		}
		
		// Dig out whatever bastard of an answer I've found
		StringBuilder result = new StringBuilder();
		int maxAnswer = 0;
		int maxIndex = 0;
		int currAnswer = 0;
		int currIndex = 0;
		for (ArrayList<Integer> x : answer) {
			currAnswer = 0;
			for (Integer y : x) {
				currAnswer += y;
			}
			if (currAnswer > maxAnswer) {
				maxAnswer = currAnswer;
				maxIndex = currIndex;
			}
			currIndex++;
			
		}
		
		for (Integer toSum : answer.get(maxIndex)) {
			result.append(toSum + " ");
		}

		return result.substring(0, result.length() - 1);
	}

	public static int computePermutation(int matrixSize, int[] permutation) {
		int sum = 0;
		for (int i = 0; i < matrixSize - 1; i++) {
			sum += matrix[permutation[i]][permutation[i + 1]];
		}
		return sum;
	}

}
